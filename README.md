# OpenML dataset: sod1_mouse

https://www.openml.org/d/41089

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Gene expression data from two separate strains of mice: C57 and 129Sv in wild type and SOD1 mutant strains.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41089) of an [OpenML dataset](https://www.openml.org/d/41089). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41089/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41089/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41089/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

